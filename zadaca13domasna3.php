<?php

function howManyTimesConsecutiveNumeberAppears($array, $number)
{
    $position =0;
    $countArray =[];
    $count = 1;
    while ($count != 0) {
        $result = howManyTimes($array, $number, $position);
        
        $count = $result['count'];
        $position = $result['position']+1;
        if ($count != 0) {
            array_push($countArray, $count);
        }
    }
  

    if (count($countArray)==0) {
        echo "Number does not appear consecutive in array";
        exit;
    }
    return $countArray;
}


function howManyTimes($array, $number, $startposition)
{
        $count =0;
        $position = $startposition;
    for ($i=$startposition; $i <count($array); $i++) {
        if (($array[$i] == $number && $position==$startposition) || ($array[$i]==$number && $position==$i-1)) {
            $count++;
            $position = $i;
        }
    }

    if ($count == 1) {
        return 0;
    }
    

    return ['count'=>$count,'position'=>$position];
}

function printTimes($array)
{
      
      $max = 0;
    for ($i=0; $i <count($array); $i++) {
        if ($array[$i] > $max){
            $max = $array[$i];
        }
    }


    return $max;
}


$niza = [1,2,3,4,5,5,5,6,7,8,5,5,5,5,9,10];

echo printTimes(howManyTimesConsecutiveNumeberAppears($niza, 5))." times"   ;

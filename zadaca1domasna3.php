<?php

    // Trial devision method http://primes.utm.edu/glossary/xpage/TrialDivision.html


function isPrime($number)
{

   //negativni broevi i 1 ne se prosti

    if ($number <= 1) {
        return false;
    }

   //2 e prost broj
    if ($number == 2) {
        return true;
    }

   //Go delime brojot so site prosti broevi pomali ili ednakvi na negoviot koren . Ako nema ostatok brojot ne e prost. Zosto ne znaeme koi se prosti broevi go delime so site

    for ($i=2; $i <=sqrt($number); $i++) {
        if ($number % $i == 0) {
            return false;
        }
    }

    return true;
}

    $number = 521;
if (isPrime($number)) {
    echo "Number ".$number." is prime";
} else {
    echo "Number ".$number." is not prime";
}

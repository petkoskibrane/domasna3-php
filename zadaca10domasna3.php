<?php

function arrayRange($array, $range)
{
    $rangeArray = [];

    if ($range[0] >= count($array) || $range[1]>=count($array)) {
        echo "Invalid range";
        exit;
    }

    for ($i=$range[0]; $i <=$range[1]; $i++) {
        array_push($rangeArray, $array[$i]);
    }

    return $rangeArray;
}

function printPositions($array)
{
    $result = "";
    for ($i=0; $i <count($array); $i++) {
        if ($i != (count($array)-1)) {
            $result.= $array[$i]." , ";
        } else {
            $result.= $array[$i];
        }
    }

    return $result;
}

$niza = [1,2,3,4,5,6,7,8,9,10];
$range = [5,9];
echo printPositions(arrayRange($niza,$range));

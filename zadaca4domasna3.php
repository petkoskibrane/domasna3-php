<?php

function checkIfNumberExists($array, $number, $range)
{

    if ($range[0] >= count($array) || $range[1] >= count($array)) {
        echo "Range is to high";
        exit;
        
    } else {
        for ($i=$range[0]; $i <=$range[1]; $i++) {
            if ($array[$i] == $number) {
                return true;
            }
        }

        return false;
    }
}


        $niza = [1,2,3,4,5,6,7,8,9,10];
        $number = 5;
        $range = [2,5];
        
if (checkIfNumberExists($niza, $number, $range)) {
    echo "Number ".$number." exist";
} else {
    echo "Number ".$number." does not exist";
}

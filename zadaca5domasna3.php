<?php

    function checkDuplicates($array)
    {
        $duplicates = [];
        
        //generating array of how many times each element apperas
        foreach ($array as $key => $value) {
            if($duplicates[$value]){
                
                $duplicates[$value] += 1;
            }else {
                $duplicates[$value] = 1;
            }
        }

        //cheching for elements that appear more than once

        foreach ($duplicates as $key => $value) {
            if($value > 1){
                return true;
            }
        }

        return false;
    }

    $niza = [1,2,3,4,5,6,5,7,2,9,10];
    if(checkDuplicates($niza)){
        echo "There are duplicates";
    }else {
        echo "There are not duplicates";
    }
?>
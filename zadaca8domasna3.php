<?php

    function indexOf($string1,$string2)
    {
        
        $index =0;
        $position =0;

        //for every character of the first string we are checking if character is first character in second string,if that is true we are cheching next character with next character in second string and we are saving position
        for ($i=0; $i<strlen($string1) ; $i++) { 
            if($string1[$i] == $string2[$index]){

                    if($index > 0){
                        //if character is not next character from the first string string does not appear in the first string
                        if($position != $i-1) return -1;
                    }
                $position = $i;
                $index++;
            }
        }
        // if number of the same characters is same as lenght of string we are returning position where string starts
        
        return (strlen($string2)==$index) ? ($position-(strlen($string2)-1)) : -1;
    }



    echo indexOf("abchellosfzdj","hello");

?>
<?php

function indexOf($string1, $string2)
{
    $position = 0;
    $result = [];

    //first we are getting the position of first appereance and then we are looking for other appereances but starting from the end of the first appereance
   
    while ($position === 0 || $position != false) {
        $position = findIndex($string1, $string2, $position);
        if ($position === 0 || $position != false) {
            array_push($result, $position-(strlen($string2)-1));
        }
    }
    
    if(count($result)==0){
        return -1;
    }
    return $result;
}


function findIndex($string1, $string2, $startposition)
{
    $index =0;
    $position =$startposition;

    //for every character of the first string we are checking if character is first character in second string,if that is true we are cheching next character with next character in second string and we are saving position
    for ($i=$startposition; $i<strlen($string1); $i++) {
        if ($string1[$i] == $string2[$index]) {
            if ($index > 0) {
                //if character is not next character from the first string string does not appear in the first string
                if ($position != $i-1) {
                    return false;
                }
            }
            $position = $i;
            $index++;
        }
    }
    // if number of the same characters is same as lenght of string we are returning position where string starts
    
    return (strlen($string2)==$index) ? $position : false;
}


function printPositions($array)
{
    $result = "";
    for ($i=0; $i <count($array); $i++) {
        if ($i != (count($array)-1)) {
            $result.= $array[$i]." and ";
        }else{
            $result.= $array[$i];            
        }
    }

    return $result;
}


    $result = indexOf("abchellosfhellozdj", "hello");
    if($result != -1){
        echo "Positions are ".printPositions($result);
    }else{
        echo $result;
    }
